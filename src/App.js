import React from "react";

/**
 * If count is 0 display "POOP_TEXT"
 * If count is divisible by 3 display "COUNTER_TEXT"
 * If count is divisible by 5 display "FOO_TEXT"
 * If count is divisible by 3 && 5 display "COUNTER_FOO_TEXT"
 * If count is not divisible by 3, 5 display "POOP_TEXT"
 * The increment button should increase the "count" variable
 * The decrement button should decrease the "count" variable
 * The "count" variable should never go below 0
 */
import { useState } from "react";

const COUNTER_TEXT = "Counter";
const FOO_TEXT = "Foo";
const COUNTER_FOO_TEXT = "🙅🏿‍♂️ Counter Foo 🙅🏿‍♂️";
const POOP_TEXT = "💩";

function App() {
  const [state, setState] = useState({ count: 15, text: POOP_TEXT });
  let count = state.count;
  let text = state.text;

  function incrementNum() {
    setState((prevState) => {
      return { ...prevState, count: prevState.count + 1 };
    });
    appText();
  }

  function decrementNum() {
    if (state.count > 0) {
      setState((prevState) => {
        return { ...prevState, count: prevState.count - 1 };
      });
    }
    appText();
  }

  function appText() {
    setState((prevState) => {
      if (prevState.count === 0) {
        return { ...prevState, text: POOP_TEXT };
      } else if (prevState.count % 3 === 0 && prevState.count % 5 === 0) {
        return { ...prevState, text: COUNTER_FOO_TEXT };
      } else if (prevState.count % 5 === 0) {
        return { ...prevState, text: FOO_TEXT };
      } else if (prevState.count % 3 === 0) {
        return { ...prevState, text: COUNTER_TEXT };
      } else {
        return { ...prevState, text: POOP_TEXT };
      }
    });
  }

  return (
    <div style={styles.app}>
      <h1>{count}</h1>
      <h3>{text}</h3>
      <div style={styles.buttons}>
        <button onClick={decrementNum}>Decrement</button>
        <button onClick={incrementNum}>Increment</button>
      </div>
    </div>
  );
}

const styles = {
  app: {
    textAlign: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
  },
};
export default App;
